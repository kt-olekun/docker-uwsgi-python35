ARG VERSION=16.04
FROM ubuntu:$VERSION
WORKDIR /var/www/python/
COPY hello.py .
RUN mkdir /var/run/socket \
&& apt-get update && apt-get install -y \
python3.5 \
python3.5-dev \
python3-pip \
&& pip3 install \
--upgrade pip \
setuptools \
uwsgi \
&& apt-get remove -y \
python3.5-dev \
python3-setuptools \
python3-pip 
CMD ["uwsgi", "--socket", "/var/run/socket/uwsgi.sock", "--wsgi-file", "hello.py", "--chmod-socket=666"]
